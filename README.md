# Model Search Examples Using Scikit-Learn and HyperOpt

* Finds the best classification model, optimizing:
  - Data pre-processing options
  - Model hyperparameters

 
- Uses Python via Jupyter Notebook
  - Libraries Required: matplotlib, numpy, pandas, pickle, sys, time
  - [Scikit-Learn](https://scikit-learn.org/stable/)
  - [Hyperopt](http://hyperopt.github.io/hyperopt/)


* Uses two datasets
  - Wine Quality by [Cortez et al. (2009)](https://www.sciencedirect.com/science/article/abs/pii/S0167923609001377?via%3Dihub)
    * Available on [Github](https://github.com/simonneutert/wine_quality_data)
  - Student Performance Data Set by [Cortez et al. (2008)](http://www3.dsi.uminho.pt/pcortez/student.pdf)
    * Available from [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/student+performance)